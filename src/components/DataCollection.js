import React from 'react';

class DataCollection extends React.Component {
    constructor ( props ) {
        super( props );

        this.state = {
            validData: false
        }
    }
  
    onChange = () => {
        // they have changed the input.  Check to see if the postal code is valid
        this.setState({validData: this.isValidPostalCode()});
    }

    onSubmit = () => {
        // the user has clicked the 'Go!' button to lookup a postal code.  Pass the info to the parent component so it can make the request for data
        this.props.onSubmit(this.fixPostalCode(this.postalCode.value));
    }

    isValidPostalCode() {
        // confirm it is a valid postal code (ie. T2Z4M8)
        return /^[A-Z][0-9][A-Z][0-9][A-Z][0-9]$/.test(this.fixPostalCode(this.postalCode.value));
    }

    fixPostalCode(postalCode) {
        // allow users to enter postal codes with spaces, hyphens or lower case characters (the api is very strict so we are trying to be more forgiving)
        return postalCode.toUpperCase().replace(/-|\s+/g,''); 
    }

    render () {
        return (
            <div className="searchArea">
                <h1>Enter your postal code to find your MP!</h1>
                <div>
                    <input ref={(postalCode) => { this.postalCode = postalCode }} className={this.state.validData ? '' : 'invalidData'}  onChange={this.onChange} type="text" placeholder="Enter Your Postal Code Here" />
                    <button className="submitBtn" onClick={this.onSubmit} disabled={!this.state.validData || this.props.processing}>Go!</button>
                </div>
            </div>
        )
    }
}

export default DataCollection;
