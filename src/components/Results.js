import React from 'react';
import '../css/Results.css';

class Results extends React.Component {
    constructor ( props ) {
        super( props );
        
        this.state = {
            validData: false,
            processing: false
        }
    }

    showError() {
        // if the postal code they entered was not found in the OpenNorth database
        return <div className="errorContainer">The postal code entered does not exist</div>
    }

    showResults() {
        // get just the respresentatives from the data
        let representatives = this.props.data.representatives_centroid;
        // now filter out just the representative that is their elected office is 'MP'
        let mp = representatives.filter(rep => rep.elected_office === "MP")[0];
        // let's pick some bits of information from the mp to display
        let { name, offices, party_name, email, district_name, personal_url } = mp;

        return (
            <div className="resultsContainer">
                <img src={mp.photo_url} alt="pic" />
                
                <div className="mpInfo">
                    <div>Name : </div><div>{name}</div>
                    <div>Party Name : </div><div>{party_name}</div>
                    <div>Email : </div><div>{email}</div>
                    <div>Phone No : </div><div>{offices[0].tel}</div>
                    <div>District Name : </div><div>{district_name}</div>
                    <div>Website : </div><div><a href={personal_url}>{personal_url}</a></div>
                </div>
            </div>
        )
    }

    render () {
        return (
            <div>
                { this.props.invalid ? this.showError() : (this.props.data ? this.showResults() : null) }
            </div>
        )
    }
}

export default Results;
