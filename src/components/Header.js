import React from 'react';
import logo from '../img/logo.svg';

const Header = () => (
    <div className="header">
        <img src={logo} className="appLogo" alt="logo" />
        <div>React Tech Challenge</div>
    </div>
);

export default Header;