import React, { Component } from 'react';
import fetchJsonp from "fetch-jsonp";
import Header from './components/Header';
import DataCollection from './components/DataCollection';
import Results from './components/Results';
import './css/App.css';

class App extends Component {
	constructor ( props ) {
		super( props );

		this.state = {
			processing: false,
			invalidCode: false,
			data: null
		}
	} 

	getRepresentativeInfo(postalCode) {
		// make a request to retreive the MP info based on the postal code entered
		this.setState( { processing: true }, function () {
			fetchJsonp('https://represent.opennorth.ca/postcodes/' + postalCode)
			.then(response => {
				return response.json()
			}).then(json => {
				this.setState({data:json, invalidCode: false, processing: false})
			}).catch(ex => {
				this.setState({invalidCode: true, processing: false})
			})
		})
	}

	render() {
		return (
			<div className="App">
				<Header />
				<DataCollection onSubmit={this.getRepresentativeInfo.bind(this)} processing={this.state.processing} />
				<Results data={this.state.data} invalid={this.state.invalidCode} />
			</div>
		);
	}
}

export default App;
